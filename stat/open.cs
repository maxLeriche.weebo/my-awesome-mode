﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper;
using Newtonsoft.Json;

namespace stat
{
    public class Open
    {
        string path = "./test";

        public static string setJSON(C test)
        {
            JsonSerializer serializer = new JsonSerializer();
            string json = JsonConvert.SerializeObject(test);
            //Console.WriteLine(json);
            return json;
        }

        public static void stockjson(C test,string path)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
            {
                file.WriteLine(setJSON(test));
            }
        }

        public static C GetJson(string path)
        {
            C retour= new C(10);
            string read= System.IO.File.ReadAllText(path);
            retour = JsonConvert.DeserializeObject<C>(read); 
            return retour;
        }

    }
}
