﻿using System;
using System.Collections.Generic;


namespace stat
{
    public class C
    {
        int[][] tabstat;
        int[] tabcontenue;
        int size = 0;
        List<int[]> etape= new List<int[]>();
        Random rnd = new Random();

        //getter/setter
        public List<int[]> Etape { get => etape; }
        public int Size { get => size; }
        public int[][] Tabstat { get => tabstat; set => tabstat = value; }
        public int[] Tabcontenue { get => tabcontenue; set => tabcontenue = value; }
        public List<int[]> GetStep() { return etape; }

        public C()
        {

        }
        public C(int size)
        {
            tabstat = new int[size][];
            tabcontenue = new int[size];
            this.size = size;
            for(int iteration =0;iteration<size;iteration++)
            {
                tabstat[iteration] = new int[size];
            }

        }
        public C(int[][] taab, int[] taabcontenue)
        {
            tabstat = taab;
            tabcontenue = taabcontenue;
            etape.Add((int[])taabcontenue.Clone());
        }

        public void Tirage()
        {
            int[] swap = new int[this.size];
            for(int iteration =0; iteration<this.size;iteration++)
            {
                if (tabcontenue[iteration]>0)
                {
                    swap = stepbystep(iteration, swap);
                }
               
            }
            etape.Add(swap);
            tabcontenue = (int[])swap.Clone();
        }

        private int[] stepbystep(int index,int[] swap)
        {
            for(int iteration =0;iteration<tabcontenue[index];iteration++)
            {
                int testc = rnd.Next(0, 100);
                int prc = 0;
                if (tabstat[index][index]>testc)
                {
                    swap[index]++;
                }
                else
                {
                    prc = tabstat[index][index];
                    int exit = 0;
                    for (int iteration2 = 0; iteration2 > this.size&&exit==0; iteration2++)
                    {
                        prc = prc + tabstat[index][iteration2];
                        if(testc<prc&&iteration2!=index)
                        {
                            swap[iteration2]++;
                            exit = 1;
                        }
                    }
                }
                
            }

            return swap;
        }


    }
}
